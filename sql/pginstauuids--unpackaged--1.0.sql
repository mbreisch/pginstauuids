-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pginstauuids FROM unpackaged" to load this file. \quit

ALTER EXTENSION pginstauuids ADD FUNCTION id_generator(shard_id int,sequence_name text, OUT result bigint) ;
