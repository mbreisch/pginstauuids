-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pginstauuids" to load this file. \quit
--The following function is an implementation of instagram's uuid generator.
--See: http://instagram-engineering.tumblr.com/post/10853187575/sharding-ids-at-instagram for details
-- The function takes two parameters:
-- 1)the id of the DB shard 2) The sequence name (you need a sequence for this to work)


CREATE OR REPLACE FUNCTION id_generator(shard_id int,sequence_name text, OUT result bigint) AS $$
DECLARE
    our_epoch bigint := 1314220021721;
    seq_id bigint;
    now_millis bigint;
    BEGIN
    SELECT nextval(sequence_name) % 1024 INTO seq_id;
SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000) INTO now_millis;
    result := (now_millis - our_epoch) << 23;
    result := result | (shard_id << 10);
    result := result | (seq_id);
END;
$$ LANGUAGE PLPGSQL;
