#OVERVIEW

pginstauuids is an implementation of Instagram's UUID generator for postgresql.

#REQUIREMENTS
pginstauuids requires postgres. It has been tested and deemed working on Postgres 9.4.

#INSTALLATION
Copy all pginstauuids* files to Postgresql's extension directory; on Debian, that is /usr/share/postgresql//extension. Then execute "CREATE EXTENSION pginstauuids;".

#USAGE
Create a schema and sequence. Use provided function "id_generator" with parameters for shard number and the name of the sequence.